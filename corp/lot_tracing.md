Lot Tracing
===========

This guide details the steps to trace inventory in INFOR.

Case 1 - Tracing inventory backwards from shipment to laminating batch/lot
--------------------------------------------------------------------------

### Example input(s)

1. Lot number `LAMNJ0000029735`.

### Steps

1. Open the **Lots** form in INFOR.
2. Enter the lot number `LAMNJ0000029735` into the **Lot** field and execute the filter.
3. Get the source document that produced this lot, in this case:
    - The source document is a `Job`
    - The job number is `110437`
    - The job suffix is `0`

  <img src="../img/lot_tracing_case01_lot_job.png" alt="Lot Tracking - Case 1 - Finding the Job" width="50%" />
4. Open the form **Material Transactions**
5. Enter the following filter and execute:
    - Set `Reference Type` to `Job`
    - Set `Ref Number` to the job number from step 3 (`110437` in this example)
    - Set `Ref Line` to the job suffix from step 3 (`0` in this example)
   The result should be the material transactions recorded against the job order.
6. Locate the subassembly item consumed immediately before the output of lot `LAMNJ0000029735` (`LAMNJ0000029734` in this example).
  <img src="../img/lot_tracing_case01_locate_immediate_lot_issued.png" alt="Lot Tracing - Case 1 - Finding immediate lot issued" width="50%">
7. Repeat steps 1 thru 6 until you get to the laminating job
    - Lot `LAMNJ0000029734` was created in job order with number `110437` and suffix `1`.
    - Filtering material transactions per step 5 should give you lot `190835` which it's the ID of the master roll.
