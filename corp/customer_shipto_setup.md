New Customer Ship-to Setup
==========================

This guide details the steps to set up a customer ship-to address.

**Important Note:** the ship-to address `0` is a special ship-to address in INFOR that is tied to the information displayed in the *Customers* form. Some fields in the *Customer Ship-Tos* are locked out because they can only be modified from the *Customers* form.

Step 1 - Add address information
--------------------------------

![](../img/customer_shipto_setup_address_tab.png "Customer Ship-Tos - Address tab")

1. Set **Name** to the ship-to name (not necessarility the same as the customer's name).
2. Set **Address [1]** to the customer's main address. Use the next (3) address fields for additional information (i.e. building No., Suite No., etc.).
3. Set **Country** to the appropriate country.
4. Set **City** to the city name.
5. Set **Prov/St** to the state or province name.
6. Set **Postal/ZIP** to the zip or postal code.
7. *Optionally*, set **County** to the county or region name.
8. *Optionally*, set **Contact** to the name of the shipping contact.
9. *Optionally*, set **Phone** to the phone number of the shipping contact.
10. *Optionally*, set **Ship To E-mail** to the e-mail address of the shipping contact. 


Step 2 - Add codes information
------------------------------

![](../img/customer_shipto_setup_codes_tab.png "Customer Ship-Tos - Codes tab")

1. Set **Warehouse** to the default warehouse that will supply customer orders to this ship-to address.
2. Set **Ship Via** to the default ship method/carrier for this ship-to address.
3. Set **Salesperson** to the salesperson that is responsible for this account's ship-to address.