Lamart Corporation
===================

List of How-To Guides
---------------------

List of how-to guides and documents are indexed by functional area below.

### Accounting

* [Customer setup](customer_setup.md)
* [Customer Ship-to setup](customer_shipto_setup.md)
* [Vendor ACH setup](vendor_ach_setup.md)
* [Vendor setup](vendor_setup.md)

### General

* [Logging into INFOR Cloudsite Industrial Desktop Client](infor_login.md)

### Engineering

* [Item Setup](item_setup.md)

### Purchasing

### Quality

* [Tracing lot numbers](lot_tracing.md)

### Sales


### Warehousing/Inventory