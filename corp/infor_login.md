Logging into INFOR CloudSuite Industrial
========================================

This guide describes the steps to log into **INFOR CloudSuite Industrial** desktop application from your corporation-issued computer (desktop or laptop).

![](../img/infor_login.png "INFOR CloudSuite Industrial Login")

1. Press `❖ WIN` key on your keyboard and search for the application named `Lamart Infor CloudSuite Industrial`.
2. Check off `Use Workstation Login`.
3. Select the site into which you want to log in.
4. Press the `OK` button to log in.