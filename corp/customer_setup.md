New Customer Account Setup
==========================

This guide describes the steps to set up a new customer account.

Step 1 - Add main information
-----------------------------

![](../img/customer_setup_main_tab.png "Customers - Main tab")

1. Leave **Customer** blank so that INFOR generates the next available customer number.
2. Set **Name** (box underneath **Customer**) to the name of the customer.
3. Set **Address [1]** to the customer's main address. Use the next (3) address fields for additional information (i.e. building No., Suite No., etc.).

    **Note:** don't add contact information here as it's not the appropriate place to store this information.
4. Set **Country** to the appropriate country.
5. *Optionally*, set the **County** to the county or region name.
6. Set **Prov/St** to the state or province.

    **Note:** contact *CSIHELP* if you need an additional state or province.
7. Set **Postal/ZIP** to the zip or postal code.
8. Set **Bank Code** to:
    - `MCH` for regular customer accounts.
    - `MHC` for HiCube customer accounts.


Step 2 - Add credit information
-------------------------------

![](../img/customer_setup_credit_tab.png "Customers - Credit tab")

1. Set **Credit Limit** to the total limit extended to the customer.
2. Set **Order Credit Limit** to the amount higher of which the system will automatically set an order on credit hold.
3. Set payment **Type** to:
    - `Check` if the customer will normally pay by check.
    - `Wire` if customer will pay by other means such as ACH, wire, credit card.
4. Set **Terms** to the appropriate terms extended to the customer. If terms are extended to a customer, by definition, a credit limit must be set (step 1).

    **Note:** contact *CSIHELP* if you need an additional terms code.


Step 3 - Add invoicing configuration
------------------------------------

![](../img/customer_setup_invoicing_tab.png "Customer - Invoicing tab")

1. Set **Invoice Category** to:
    - `DEFAULTCATEGORY` if it's a regular customer.
    - `HICUBE` if it's a HiCube customer.


Step 4 - Add contacts information
---------------------------------

![](../img/customer_setup_contacts_tab.png "Customer - Invoicing tab")

1. Set **Order Contact** to the name of the main order contact in the customer's company.
2. Set **Phone** to the phone number of the main order contact.
3. Set **Order E-mail** to the e-mail address of the main order contact.
4. Set **Billing Contact** to the name of the main billing contact in the customer's company.
5. Set **Phone** to the phone number of the billing contact.
6. Set **Billing E-mail** to the e-mail address of the billing contact.


Step 5 - Add codes
------------------

![](../img/customer_setup_codes_tab.png "Customer - Codes tab")

1. Set **End User Type** to the appropriate code. Please note that:
    - Regular account codes begin with `L`.
    - HiCube account codes begin with `H`.

    **Note:** contact *CSIHELP* if you need an additional end user type code.