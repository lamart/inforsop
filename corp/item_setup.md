Setting up a new inventory item
==============================================

This guide details the steps to set up an inventory item.

Case 1 - Setting up a purchased item
------------------------------------

Open the **Items** form and follow the steps below.

### Step 1 - Add basic information (*Main tab*)

![](../img/item_setup_purch_main_tab.png "Items - Main Tab (Purchased)")

1. Set **Item** to the item number.
2. Set **Description** to the item description.
3. Set **U/M** to the base unit of measure.
4. Set **Type** to `Material`.
5. Set **Source** to either `Purchased`.
6. Set **Product Code** to an appropriate code that begins with an `R-` prefix.
7. Set **Cost Type** to `Standard`.
8. Set **Cost Method** to `Standard`.

### Step 2 - Add controls (*Controls tab*)

![](../img/item_setup_purch_controls_tab.png "Items - Controls Tab (Purchased)")

1. Check off **Lot Track** if you want to lot-track inventory; uncheck otherwise.
2. Set **Lot Prefix** to `LAMPO` if inventory is lot-tracked (see step 9).
3. Set **Material Status** to `Obselete`, pending review.
4. Set **Reason** to `NRY` ("Not ready"), pending review.

### Step 3 - Add attributes (*Attributes tab*)

![](../img/item_setup_purch_attributes_tab.png "Items - Attributes Tab (Purchased)")

1. Set **Item Attribute Group** to `LAM_ITM`.
2. Set **Lot Attribute Group** to `LAM_LOT` if inventory is lot-tracked; leave *blank* otherwise.
3. Set **LC Classification** attribute to `RAW`.


Case 2 - Setting up a manufactured item
---------------------------------------

Open the **Items** forms and follow the steps below.

### Step 1 - Add basic information (*Main tab*)

![](../img/item_setup_mfct_main_tab.png "Items - Main Tab (Manufactured)")

1. Set **Item** to the item number.
2. Set **Description** to the item description.
3. Set **U/M** to the base unit of measure.
4. Set **Type** to `Material`.
5. Set **Source** to either `Manufactured`.
6. Set **Product Code** to an appropriate code that begins with an `S-` or `F-` prefix.
7. Set **Cost Type** to `Standard`.
8. Set **Cost Method** to `Standard`.

### Step 2 - Add controls (*Controls tab*)

![](../img/item_setup_mfct_controls_tab.png "Items - Controls Tab (Manufactured)")

1. Check off **Lot Track** if you want to lot-track inventory; uncheck otherwise.
2. Set **Lot Prefix** to `LAMNJ` if inventory is lot-tracked (see step 9).
3. Set **Material Status** to `Obselete`, pending review.
4. Set **Reason** to `NRY` ("Not ready"), pending review.

### Step 3 - Add attributes (*Attributes tab*)

![](../img/item_setup_mfct_attributes_tab.png "Items - Attributes Tab (Manufactured)")

1. Set **Item Attribute Group** to `LAM_ITM`.
2. Set **Lot Attribute Group** to `LAM_LOT` if inventory is lot-tracked; leave *blank* otherwise.
3. Set **LC Classification** attribute to:
    - `SUB` if inventory is subassembly.
    - `FG` if inventory is finished good.
    - `DEV` if inventory is developmental.