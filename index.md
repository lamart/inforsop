INFOR Online Help
=================

Welcome to the **INFOR Online Help** website for Lamart Corporation.

This website is maintained by the *Management Information Systems* department. If you find any issues or errors please contact the maintainers [here](mailto:csihelp@lamartcorp.com?subject=INFOR%20Online%20Help%20Issue).