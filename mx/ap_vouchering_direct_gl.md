A/P Vouchering - Direct G/L Account
===================================

This files describes the steps necessary to enter and post an A/P voucher (invoice) directly into the system. This method does not require that a purchase order be present in the system already with a receipt already registered against---thi is what it's meant by "direct". **Please note that this should be more the exception than the rule as it's expected that the majority of Lamart's vouchers should originate from an approved purchase order.**

A/P vouchering in INFOR is a 3-step process:

1. Enter A/P Voucher.
2. Generate and review distributions.
3. Post A/P Voucher.

Step 1: Enter A/P Voucher
-------------------------

1. Open the form *A/P Vouchers and Adjustments*.
2. Press the `[F4]` key to show all open vouchers. Press the new button on the top menu to create a new voucher.
3. Enter the vendor number in the **Vendor** field.
4. Make sure the **Type** field is set to "Voucher" (this is the default).
5. Click the `[Save]` button to generate the next voucher number which the system will insert into the **Voucher** field.
6. Enter the vendor's invoice number in the **Invoice** field.
7. Enter the invoice date in the **Invoice Date** field.
8. Enter the distribution (posting) date in the **Distribution Date** field, which should be the same as the invoice date for the most part.
9. Enter the invoice subtotal into the **Purch Amt** field.
10. Enter total freight charge, if any, into the **Freight** field.
11. Enter total sales tax, if any, into the **Sales Tax** field.
12. Enter total of other charges, if any, into the **Misc Charges** field.
13. For the most part, you will not have the below charges in vouchers so leave them at `0.00`:
    * Duty
    * Brokerage
    * Insurance
    * Local Freight
14. Confirm that the discount date in the **Disc Date** field matches the terms in the invoice received.
15. Confirm that the due date in the **Due Date** field matches the terms in the invoice received.
16. Confirm the currency in the **Currency** field.
17. Confirm that a G/L account number has been defined in the **A/P Acct** field. If it's blank, the vendor account is not correctly defined.
18. Select the appropriate tax code in the **Tax Code** field: this code is copied from the vendor account.
19. Click on the `[Save]` button to save the voucher data.
20. You're done with this step; go to the next one.

![](/img/ap_vouchering_direct_gl.png "A/P Vouchers and Adjustments Form")


Step 2: Generate and Review Distributions
-----------------------------------------

1. In the same form *A/P Vouchers and Adjustments* click on the `[Distribution Generation]` button on the right.
2. Click the `[OK]` button on the pop-up question "[Distribution Generation] will be performed".
3. Click the `[OK]` button on the pop-up window that acknowledges the system has generated the distribution lines. If the total tax amount entered in the **Sales Tax** field differs from the tax amount calculated by the system based on the tax code in the **Tax Code** field, you will see error messages at this step. The system will create the distribution tax lines based on the tax code and advise that they be adjusted manually to match the total sales tax. If you made a mistake entering the sales tax amount in the **Sales Tax** field, update it and save. **Note that you don't have to regenerate the distributions lines because of this error.**
4. Click on the `[Distribution]` button on the right to go to the generated distribution lines.
5. For each distribution line that is not tax:
    
    1. Verify the line amount in the **Amount** field.
    2. Verify/Update G/L account number in the **Account** field and verify/enter/update the unit codes 1 thru 4, if appropriate or able, in the four boxes adjacent to this field.
    3. Save your udpated data.

    The system will generate a single line for total invoice subtotal amount entered in the **Purch Amt** field in the previous step. If you need to split this amount into several different G/L accounts, do the following:

        1. Revise down the amount in the **Amount** field of the generated line.
        2. Add a new distribution line by clicking on the `[New]` button.
        3. Enter the line amount in the **Amount** field.
        4. Enter the G/L account number in the **Account** field and enter the unit codes 1 thru 4, if appropriate or able, in the four boxes adjacent to this field.
        5. Save your updated data.
        6. Repeat steps 2-5 until you don't have any balance left to be distributed.


6. Verify or manually update the tax amount on the tax line if needed. You will have to do this if not all the invoice lines are taxable as the system will calculate tax on the full purchase amount.
7. Once all lines have been verified or updated, close the form *Voucher Adjustment Distribution*.
8. If you receive a warning message that not all amounts have been distributed, it means that the line breakdowns don't add up to the amounts that were entered in the voucher header in the previous steps. In this case, either:
    1. Go back to the voucher header and make sure you did not make a mistake in the header amounts; or
    2. Go back to the distribution lines and make sure you did not make a mistake in the lines.
9. You're done with this step; go to the next one to post the A/P voucher.


Step 3: Post A/P Voucher
------------------------

1. Click on the `[A/P Voucher Posting]` button on the right of the *A/P Vouchers and Adjustments* form, which opens up the *A/P Voucher Posting* form.
2. Click on the `[Process]` button to print the voucher transaction.
3. Select the "Commit" option and click on the `[Process]` button to commit the voucher into the A/P distribution journal.
4. If the system doesn't find any errors with the voucher, you will received a success message acknowledging that the distrubtion lines were committed. The system will not post your voucher if:
    * Your distribution line amounts don't add up to your header amounts, or
    * You're missing required unit codes in a G/L account in any of the distribution lines, or
    * You're not allowed to post to the date in the **Distribution Date** field.

Note: after step 3, the distribution lines will be moved to the A/P distribution journal pending ledger posting. The general ledger is not affected until this step is finalized. It's not part of this SOP for of 2 reasons:    
    1. It can be done at the end of the day once all vouchers for the day have been posted.
    2. It should be done by an accounting user that understands the accounting processes of the system which may not be the case of the user who enters A/P vouchers, for the most part.