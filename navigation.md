# INFOR Online Help

[Lamart Corporation]()

  * [Home](corp/corp.md)
  - - - -
  * [Item Setup](corp/item_setup.md)
  * [Vendor ACH setup](corp/vendor_ach_setup.md)
	
[Lamart California]()

  * [Home](cal/cal.md)
  - - - -
  * [Order Entry](cal/order_entry.md)
  * [Transfer Orders](cal/transfer_orders.md)
  * [Non-Inventory Customer Order Lines - G/L Mapping](cal/customer_orders_non-inventory_lines_gl_mapping.md)
  * [Non-Inventory Purchase Order Lines - G/L Mapping](cal/purchase_orders_non-inventory_lines_gl_mapping.md)

[Lamart Mexico]()

  * [Home](mx/mx.md)
  - - - -

[About]()

  * [Updates Timeline](updates.md)
  
