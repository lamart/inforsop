Updates Timeline
================

Log of changes to content in this website.

### Update - Sunday March 3, 2019

1. Added **customer setup** guide (☑ SLAMNJ,  ☑ SLAMCA).
2. Added **customer ship-to setup** guide  (☑ SLAMNJ,  ☑ SLAMCA).
3. Added **vendor setup** guide (☑ SLAMNJ,  ☑ SLAMCA).
4. Added **vendor ACH setup** guide (☑ SLAMNJ,  ☑ SLAMCA).
5. Added instructions to log into INFOR desktop client.
