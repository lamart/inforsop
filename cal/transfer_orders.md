Material Transfer Orders
========================

To complete a transfer order you must complete all 3 steps 

  
 
Step 1 - Create a Transfer Order
--------------------------------

![](/img/transfer_orders.png "Transfer Orders Form")

1. Enter a new transfer order number
    1. The format is T#-0MMDDYY (example T1-0031218, for Truck 1 of March 12, 2018)
2. The from site is SLAMCA
3. The from Whse is OTAY
4. The to site is SLAMMX
5. The to Whse is MEX
6. Click Save
7. Click on Lines

![](/img/transfer_order_lines.png "Transfer Order Lines Form")

1. Chose the item
2. Click ok on this message ![](/img/transfer_order_lines_price.png "Click on on this message")
3. Enter the quantity
4. Select the schedule date in the Sched Ship Date field (this date should match the transfer order number)
5. Enter the Sched received date (should be the following business day)
6. Verify the transit loc field is set to Transit
7. Select Inventory as the source
__You can add as many lines as needed__


Step 2 - Ship Transfer Order
----------------------------

![](/img/transfer_order_ship.png "Transfer Order Ship")

1. Open transfer order ship and select the transfer order you want to ship
2. The system will propose Locations and lot number, you can click the dropdown list in location to see how much is available per location
    1. ![](/img/transfer_order_from_location.png "")
3.	You will then be able to see how much you have of each lot in that location
    1. ![](/img/transer_orders_lot_qty.png "")
4. ![](/img/transfer_order_select_lines.png "")
5. If you change the lot the system will adjust the available quantity but it will not remove the additional lines it created to this item you must not select them
6. Click on Ship


Step 3 - Ship Transfer Order
----------------------------

![](/img/transfer_order_receive.png "Transfer Order Receive")

1. Open the transfer order receive form
2. Select each line
3. Set location to QC-RECEIVE
4. Click receive
5. ![](/img/transfer_order_receive_confirm.png "Confirmation prompt")
6. Click Ok
