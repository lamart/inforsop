New Vendor Account Setup
========================

This guide describes the steps to set up a new vendor account.

Step 1 - Add main information
-----------------------------

![](../img/vendor_setup_main_tab.png "Vendors - Main tab")

1. Skip **Vendor** and let INFOR generate the next available vendor number.
2. Enter the vendor name in the box underneath **Vendor**
3. Set **Category** as follows:
    - `EMP` for Lamart employee vendor account.
    - `ICO` for an inter-company account.
    - `NTR` for a non-trade vendor (GL-based purchases).
    - `TRD` for a trade vendor (supplies proper inventory items).
4. Set **Address [1]** to the vendor address. Use the next (3) address fields for additional information (i.e. building No., Suite No., etc.).

    **Note:** don't add contact information here as it's not the appropriate place to store this information.
5. Set **Country** to the country name.
6. *Optionally*, set **County** to the county name.
7. Set **Prov/St** to the state or province code.

    **Note:** contact *CSIHELP* if you need an additional state or province.
8. Set **City** to the city name.
9. Set **Postal/ZIP** to the postal or zip code.
10. Set **Terms** to appropriate terms code.

    **Note:** contact *CSIHELP* if you need an additional terms code.
11. Set **Currency** to `USD`.
12. Set **Bank Code** to `MCH`.
12. *Optionally*, if the vendor is also a customer, set **Customer** to the customer number to link the accounts.
13. Set **Warehouse** to `OTAY`.
14. Set **Language** to `ENU`.


Step 2 - Add miscellaneous/general information
----------------------------------------------

![](../img/vendor_setup_general_tab.png "Vendors - General tab")

1. *Optionally*, set **Purchases** to the default ledger account to use when distributing vouchers (this is appropriate for vendors for which ledger-based purchase orders are used).
2. If default purchasing ledger account was defined in step 1, fill out the (4) boxes to the right with the appropriate **Unit Code 1**, **Unit Code 2**, **Unit Code 3**, and **Unit Code 4** (mandatory fields will appear with a yellow background).
3. Set **Ship Via** to the default shipping carrier/method.
4. Set **FOB** as follows:
    - `Destination, Freight Prepaid` if supplier pays the freight charges and owns the goods while in transit.
    - `Destination, Freight Collect` if Lamart pays the freight charges but the supplier owns the goods while in transit.
    - `Destination, Freight Collect and Allowed` if Lamart pays the freight charges, but deducts the costs from supplier's invoice and supplier owns the goods while in transit.
    - `Origin` or `Shipping Point` if Lamart owns the goods while in transit (after goods leave supplier's dock).
5. Set **Payment Type** as follows:
    - `Standard Check` if we will pay supplier by INFOR-generated check.
    - `Manual Check` if we will pay supplier by manual check.
    - `Wire` if we will pay supplier by wire.
    - `EFT` if we will pay supplier by ACH (only within the United States).

    **Note:** These payments types are defaults and can always be changed when entering payments.

6. If payment type is set to `EFT` in previous step, set **EFT Format** to `NACHA CCD`.


Step 3 - Add vendor banking configuration
-----------------------------------------

![](../img/vendor_setup_banking_tab.png "Vendors - Banking tab")

This section is fill out if the supplier is paid by ACH. Please refer [this guide](vendor_ach_setup.md) if setting up vendor for ACH payment.


Step 4 - Add contacts information
---------------------------------

![](../img/vendor_setup_contacts_tab.png "Vendors - Contacts tab")

1. Set **Contact** to the full name of the main order contact.
2. Set **Phone** to the phone number of the main order contact.
3. *Optionally*, set **Fax** to the fax number of the main order contact.
4. *Optionally*, set **Order E-mail** to the e-mail address of the main order contact.
5. *Optionally*, set **Billing E-mail** to the e-mail address of the main billing contact.