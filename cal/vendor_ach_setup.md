Setting up a vendor for ACH payment processing
==============================================

This guide details the steps to set up a vendor account for ACH payment processing.

Step 1 - Setup payment type and bank information
------------------------------------------------

![](../img/vendor_ach_setup_general_tab.png "Vendors - General tab")

1. Set **Payment Type** to `EFT`
2. Set **EFT Format** to `NACHA CCD`

   ![](/img/vendor_ach_setup_banking_tab.png "Vendors - Banking tab")

3. Set **[ Vendor Bank]** to appropriate bank from pull down.
4. Verify number in **Bank Transit Number** matches the first 8 digits of the Rounting/ABA No. provided by vendor. If not matched, go back to step 3 and select appropriate bank.
5. Set **Transit Reference** to `01`
6. Set **Bank Account** to bank accounting number provided by vendor.
7. Set **Account Type** to appropriate value provided by vendor.

Configure automations next. From right-side menu, click on **Vendor Document Profile** button for the next steps.

![](../img/vendor_ach_setup_sidemenu_docprofile.png "Vendor Document Profile side-menu.")

Step 2 - Setup ACH remittance email automation
----------------------------------------------

![](../img/vendor_ach_setup_docprofile_email.png "Setting up ACH Remittance E-mail")

Add a new record as follows:

1. Set **Method** to `E-Mail` from drop-down.
2. Set **Destination** to list of e-mail addresses (separated by semicolons without spaces) for all recipients requested by supplier. Add "payables.lamca@lamartcorp.com" at the end.
    - Example: to send email to *john.doe@somecompany.com* set this field as `john.doe@somecompany.com;payables.lamca@lamartcorp.com`.
3. Set **Task Name** to `A/P EFT Posting - Remittance Advice`.
4. Check off **Active** to activate entry.

Step 3 - Setup ACH remittance print automation
----------------------------------------------

![](../img/vendor_ach_setup_docprofile_printer.png "Setting up ACH Remittance E-mail")

Add a new record as follows:

1. Set **Method** to `Printer` from drop-down.
2. Set **Destination** to `ACCOUNTING`
3. Set **Task Name** to `A/P EFT Posting - Remittance Advice`.
4. Check off **Active** to activate entry.