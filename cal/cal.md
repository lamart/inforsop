Lamart California
=================

List of How-To Guides
---------------------

List of how-to guides and documents are indexed by functional area below.

### Accounting

* [Customer setup](customer_setup.md)
* [Customer Ship-to setup](customer_shipto_setup.md)
* [Vendor ACH setup](vendor_ach_setup.md)
* [Vendor setup](vendor_setup.md)

### General

* [Logging into INFOR Cloudsite Industrial Desktop Client](../corp/infor_login.md)

### Purchasing

* [Booking non-inventory purchase order lines](purchase_orders_non-inventory_lines_gl_mapping.md)

### Sales

* [Booking non-inventory customer order lines](customer_orders_non-inventory_lines_gl_mapping.md)

### Warehousing/Inventory

* [Processing material transfer orders from OTAY to Ensenada](transfer_oders.md)