Purchase Orders - G/L Mapping of Non-Inventory Lines
====================================================

This file is a guide for booking non-inventory purchase order lines.

How to read the table below:

1. **Non-Inventory Item**: brief description of the good/service being booked.
2. **Type**: the kind of order line to be used. It's either "G/L" for ledger-based or "N/S" for non-stock item.
3. **Number**: the G/L account number or non-stock item number, depending on the value in the "Type" column.
4. **UC1 thru UC4**: notes on the unit codes 1 thru 4
    * "RQ" means that the code is required. In parenthesis, you will read either "Pick" which means you have to pick from the pull-down list or you will read an actual code which you must use.
    * "OP" means that the code is optional.
    * *Blank* means that the code is not required.

| Non-Inventory Item                     | Type | Number | UC1       | UC2 | UC3       | UC4 |
|:---------------------------------------|:-----|:-------|:----------|:----|:----------|:----|
| Packaging: boxes                       | G/L  | 53280  | 2225      |     |           |     |
| Packaging: cores                       | G/L  | 53280  | 2225      |     |           |     |
| Packaging: cornerboards                | G/L  | 53280  | 2225      |     |           |     |
| Packaging: release disks               | G/L  | 53280  | 2225      |     |           |     |
| Packaging: shrink film                 | G/L  | 53280  | 2225      |     |           |     |
| Pallets (any kind)                     | G/L  | 53280  | 2225      |     |           |     |
| Production machine parts               | G/L  | 53040  | RQ (Pick) |     | RQ (Pick) |     |
| QC Cert Training                       | G/L  | 53120  | RQ (Pick) |     |           |     |
| Rubber dies for stamping in production | G/L  | 53030  |           |     |           |     |
| Sales tax                              | G/L  | 65050  | RQ (Pick) |     |           |     |
