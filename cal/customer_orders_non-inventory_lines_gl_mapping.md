Customer Orders - G/L Mapping of Non-Inventory Lines
====================================================

This file is a guide for booking non-inventory customer order lines.

Non-Inventory Item | Type | G/L Account
-------------------|------|------------
Sales tax          | G/L  | 21010